require 'legion/extensions/plex/version'

module Legion
  module Extensions
    module Plex
      extend Legion::Extensions::Core if Legion::Extensions.const_defined? :Core
    end
  end
end
